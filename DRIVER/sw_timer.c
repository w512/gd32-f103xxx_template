#include "sw_timer.h"
//#include "stdio.h"

/*******************************************************************************
1. 宏定义
*******************************************************************************/

/*******************************************************************************
2. 全局变量定义
*******************************************************************************/
static soft_timer_t timer_list[SOFT_TIMERS_MAX];
static volatile uint32_t ul_tick_cnt = 0;    //软件定时器时钟节拍


/*******************************************************************************
3. 函数定义
*******************************************************************************/
/*****************************************************
* function: 更新时钟节拍
* param:
* return:
* note:     需在定时器中断内执行
******************************************************/
void tick_cnt_update(void)
{
    ul_tick_cnt++;
}

/*****************************************************
* function: 获取时钟节拍
* param:
* return:   时钟节拍
* note:
******************************************************/
uint32_t tick_cnt_get(void)
{
    return ul_tick_cnt;
}

//定时器2中断服务程序
void TIMER2_IRQHandler(void)
{
    if (timer_interrupt_flag_get(TIMER2, TIMER_INT_FLAG_UP) != RESET) { // 检查TIM2更新中断发生与否
        /* clear channel 0 interrupt bit */
        timer_interrupt_flag_clear(TIMER2, TIMER_INT_FLAG_UP);  // 清除TIMx更新中断标志

        tick_cnt_update();
    }
}

void soft_timer_init(void)
{
    uint8_t uc_idx;

    for (uc_idx = 0; uc_idx < SOFT_TIMERS_MAX; uc_idx++)
    {
        timer_list[uc_idx].en_timer_mode  = MODE_ONE_SHOT;
        timer_list[uc_idx].en_timer_state = SOFT_TIMER_STOPPED;
        timer_list[uc_idx].ul_period = 0;
        timer_list[uc_idx].ul_timeout = 0;
        timer_list[uc_idx].cb = NULL;
        timer_list[uc_idx].param = NULL;
    }
}

void soft_timer_start(uint16_t us_timer_id, timer_mode en_mode, uint32_t ul_delay, soft_timer_callback *cb, void *param)
{
//    assert_param(us_timer_id < SOFT_TIMERS_MAX);
//    assert_param(en_mode == MODE_ONE_SHOT || en_mode == MODE_PERIODIC);

#if 0
    timer_list[us_timer_id].ul_timeout = tick_cnt_get() + ul_delay;
#endif
    timer_list[us_timer_id].ul_timeout = ul_delay;

    timer_list[us_timer_id].ul_period = ul_delay;
    timer_list[us_timer_id].en_timer_state = SOFT_TIMER_RUNNING;
    timer_list[us_timer_id].en_timer_mode = en_mode;
    timer_list[us_timer_id].cb = cb;
    //timer_list[us_timer_id].argv = argv;
    timer_list[us_timer_id].param = param;
}


void soft_timer_update(void)
{
    uint16_t us_idx;

#if 0
    for(us_idx = 0; us_idx < SOFT_TIMERS_MAX; us_idx++)
    {
        switch (timer_list[us_idx].en_timer_state)
        {
            case SOFT_TIMER_STOPPED:
                break;

            case SOFT_TIMER_RUNNING:
                if (timer_list[us_idx].ul_timeout <= tick_cnt_get())
                {
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_TIMEOUT;
                    timer_list[us_idx].cb(timer_list[us_idx].param);       //执行回调函数
                }
                break;

            case SOFT_TIMER_TIMEOUT:
                if (timer_list[us_idx].en_timer_mode == MODE_ONE_SHOT)
                {
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_STOPPED;
                }
                else
                {
                    timer_list[us_idx].ul_timeout = tick_cnt_get() + timer_list[us_idx].ul_period;
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_RUNNING;
                }
                break;

            default:
                break;
        }
    }
#endif
    for(us_idx = 0; us_idx < SOFT_TIMERS_MAX; us_idx++)
    {
        switch (timer_list[us_idx].en_timer_state)
        {
            case SOFT_TIMER_STOPPED:
                break;

            case SOFT_TIMER_RUNNING:
                if (timer_list[us_idx].ul_timeout == 0)
                {
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_TIMEOUT;
                    timer_list[us_idx].cb(timer_list[us_idx].param);       //执行回调函数
                }
                else
                {
                    timer_list[us_idx].ul_timeout--;
                }
                break;

            case SOFT_TIMER_TIMEOUT:
                if (timer_list[us_idx].en_timer_mode == MODE_ONE_SHOT)
                {
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_STOPPED;
                }
                else
                {
                    timer_list[us_idx].ul_timeout = timer_list[us_idx].ul_period;
                    timer_list[us_idx].en_timer_state = SOFT_TIMER_RUNNING;
                }
                break;

            default:
                break;
        }
    }

}

void soft_timer_stop(uint16_t us_timer_id)
{
//    assert_param(us_timer_id < SOFT_TIMERS_MAX);
    timer_list[us_timer_id].en_timer_state = SOFT_TIMER_STOPPED;
}

timer_state soft_timer_get_state(uint16_t us_timer_id)
{
    return timer_list[us_timer_id].en_timer_state;
}
