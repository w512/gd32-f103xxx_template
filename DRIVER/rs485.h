#ifndef __RS485_H__
#define __RS485_H__

#include "gd32f10x.h"

extern uint8_t g_rs485_tx_buffer[20];         // 发送缓冲,最大20个字节

void RS485_RecvData(uint8_t *buff, uint8_t *len);
void RS485_SendData(uint8_t *buff, uint8_t len);
void RS485_RecvDataHandle(uint8_t *buff, uint8_t len);
unsigned short RS485_CrcCheck(unsigned char *data, unsigned short len);
#endif // end of __RS485_H__
