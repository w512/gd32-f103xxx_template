#include "rs485.h"
#include "usart_lcd.h"
#include "systick.h"

#define SHOW_0_FLOOR    0x1300
#define SHOW_1_FLOOR    0x1301
#define SHOW_2_FLOOR    0x1302
#define SHOW_3_FLOOR    0x1303
#define SHOW_4_FLOOR    0x1304
#define SHOW_5_FLOOR    0x1305
#define SHOW_6_FLOOR    0x1306
#define SHOW_7_FLOOR    0x1307
#define SHOW_8_FLOOR    0x1308
#define SHOW_9_FLOOR    0x1309
#define SHOW_b1_FLOOR   0x1201   //-1
#define SHOW_b2_FLOOR   0x1202   //-2
#define SHOW_b3_FLOOR   0x1203   //-3
#define SHOW_b_1_FLOOR  0x0b01
#define SHOW_b_2_FLOOR  0x0b02
#define SHOW_b_3_FLOOR  0x0b03
#define SHOW_F_FLOOR    0x1A01   //F1

uint16_t g_rs485_rx_data_cnt = 0;
uint8_t g_rs485_rx_buffer[64];         // 接收缓冲,最大64个字节
uint8_t g_rs485_tx_buffer[20];         // 发送缓冲,最大20个字节

void USART1_IRQHandler(void)
{
    uint8_t data;

    if (RESET != usart_interrupt_flag_get(USART1, USART_INT_FLAG_RBNE))
    {
        data = usart_data_receive(USART1);

        usart_interrupt_flag_clear(USART1, USART_INT_FLAG_RBNE);
        // if(res == 0x3f) g_rs485_rx_data_cnt = 0;
        // gauc_rs485_rx_buf[g_rs485_rx_data_cnt++] = res;
        if (g_rs485_rx_data_cnt < 64) {
            g_rs485_rx_buffer[g_rs485_rx_data_cnt] = data;  //记录接收到的值
            g_rs485_rx_data_cnt++;                          //接收数据增加1
        }
    }
}

void RS485_RecvData(uint8_t *buff, uint8_t *len)
{
    uint8_t rx_len = g_rs485_rx_data_cnt;
    uint8_t idx = 0;

    *len = 0;   // 默认为0

    /* 等待10ms,连续超过10ms没有接收到一个数据, 则认为接收结束 */
    delay_ms(10);

    if (rx_len == g_rs485_rx_data_cnt && rx_len) // 接收到了数据,且接收完成了
    {
        for (idx = 0; idx < rx_len; idx++)
        {
            buff[idx] = g_rs485_rx_buffer[idx];
        }

        *len = g_rs485_rx_data_cnt;  // 记录本次数据长度
        g_rs485_rx_data_cnt = 0;     // 清零
    }
}

void RS485_SendData(uint8_t *buff, uint8_t len)
{
    uint8_t idx;

    // 设置为发送模式
    gpio_bit_write(GPIOA, GPIO_PIN_15, SET);

    for (idx = 0; idx < len; idx++)      //循环发送数据
    {
        while(usart_flag_get(USART1, USART_FLAG_TC) == RESET);
        usart_data_transmit(USART1, buff[idx]);
    }

    while(usart_flag_get(USART1, USART_FLAG_TC) == RESET);

    g_rs485_rx_data_cnt = 0;

    //设置为接收模式
    gpio_bit_write(GPIOA, GPIO_PIN_15, RESET);
}

unsigned short RS485_CrcCheck(unsigned char *data, unsigned short len)
{
    unsigned short i;
    unsigned short us_crcValue = 0xFFFF;

    while (len--)
    {
        us_crcValue ^= *data++;
        for (i = 0; i < 8; i++)
        {
            if (us_crcValue & 0x0001)
            {
                us_crcValue = (us_crcValue >>1) ^ 0xa001;
            }
            else
            {
                us_crcValue=us_crcValue >> 1;
            }
        }
    }

    return (us_crcValue);
}

void RS485_reply_92()
{
    uint16_t crc_val;

    g_rs485_tx_buffer[0] = 0x5C;
    g_rs485_tx_buffer[1] = 0x01;
    g_rs485_tx_buffer[2] = g_ctrl_info.floor_btn[0];
    g_rs485_tx_buffer[3] = g_ctrl_info.floor_btn[1];
    g_rs485_tx_buffer[4] = g_ctrl_info.floor_btn[2];
    g_rs485_tx_buffer[5] = g_ctrl_info.floor_btn[3];
    g_rs485_tx_buffer[6] = g_ctrl_info.floor_btn[4];
    g_rs485_tx_buffer[7] = g_ctrl_info.floor_btn[5];
    g_rs485_tx_buffer[8] = g_ctrl_info.floor_btn[6];
    g_rs485_tx_buffer[9] = (g_ctrl_info.light_ctrl_btn << 1) | g_ctrl_info.fan_ctrl_btn;

    crc_val = RS485_CrcCheck(g_rs485_tx_buffer, 10);
    g_rs485_tx_buffer[10] = (crc_val >> 8) && 0xFF;
    g_rs485_tx_buffer[11] = crc_val && 0xFF;

    RS485_SendData(g_rs485_tx_buffer, 12);

    g_ctrl_info.floor_btn[0] = 0;
    g_ctrl_info.floor_btn[1] = 0;
    g_ctrl_info.floor_btn[2] = 0;
    g_ctrl_info.floor_btn[3] = 0;
    g_ctrl_info.floor_btn[4] = 0;
    g_ctrl_info.floor_btn[5] = 0;
    g_ctrl_info.floor_btn[6] = 0;
}

void RS485_BroadcastFrameHandle(uint8_t *buff,uint8_t uc_len)
{
    uint8_t uc_H_byte;
    uint8_t uc_L_byte;
    uint8_t header;
    uint16_t show_mode;
    uint8_t  show_floor_num;
    uint8_t  show_direction;

    uc_L_byte =(buff[2] >> 2) & 0xff;
    uc_H_byte = buff[3] & 0xff;
    header = buff[0];
    show_mode = ((uc_H_byte & 0x3F) << 8) | (uc_L_byte & 0x3F);//((uc_H_byte << 8) | (uc_L_byte ));

    if (header == 0x5A)
    {
        switch (show_mode)
        {
            case SHOW_0_FLOOR:
                break;
            case SHOW_1_FLOOR:
                show_floor_num = 1;
                break;
            case SHOW_2_FLOOR:
                show_floor_num = 2;
                break;
            case SHOW_3_FLOOR:
                show_floor_num = 3;
                break;
            case SHOW_4_FLOOR:
                show_floor_num = 4;
                break;
            case SHOW_5_FLOOR:
                show_floor_num = 5;
                break;
            case SHOW_6_FLOOR:
                show_floor_num = 6;
                break;
            case SHOW_7_FLOOR:
                show_floor_num = 7;
                break;
            case SHOW_8_FLOOR:
                show_floor_num = 8;
                break;
            case SHOW_9_FLOOR:
                show_floor_num = 9;
                break;
            case SHOW_b1_FLOOR:
                show_floor_num = 11;
                break;
            case SHOW_b2_FLOOR:
                show_floor_num = 12;
                break;
            case SHOW_b3_FLOOR:
                show_floor_num = 13;
                break;
            case SHOW_b_1_FLOOR:
                show_floor_num = 14;
                break;
            case SHOW_b_2_FLOOR:
                show_floor_num = 15;
                break;
            case SHOW_b_3_FLOOR:
                show_floor_num = 16;
                break;
            default:
                show_floor_num = 0;
        }

        // 显示楼层
        USART_LCD_ShowFloorNum(show_floor_num);

        // 显示方向箭头
        show_direction = buff[1] ;
        USART_LCD_ShowDirArrow(show_direction);
    }

    if (header == 0x5C)
    {
        g_floor_1_btn = buff[2] & 0x01;
        g_floor_2_btn = buff[2] & 0x02;
        g_floor_3_btn = buff[2] & 0x04;
        g_floor_4_btn = buff[2] & 0x08;
        g_floor_5_btn = buff[2] & 0x10;
        g_floor_6_btn = buff[2] & 0x20;
        g_open_door_btn = buff[1] & 0x20;
        g_close_door_btn = buff[1] & 0x40;

        RS485_reply_92();
    }
}

void RS485_RecvDataHandle(uint8_t *buff, uint8_t len)
{
    if (len >= 8) // 表示广播帧
    {
        RS485_BroadcastFrameHandle(buff, len);
    }
    else
    {
    }
}
