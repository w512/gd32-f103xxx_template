#ifndef __SW_TIMER_H
#define __SW_TIMER_H

#include "gd32f10x.h"

#define SOFT_TIMERS_MAX 10

typedef void soft_timer_callback(void *param);

/******************************************************************************
1. 宏定义
*******************************************************************************/
/* 软件定时器时钟节拍单位 */
#define TIME_BASE_MS    1    //1ms


/******************************************************************************
2. 枚举定义
*******************************************************************************/
typedef enum timer_mode {
    MODE_ONE_SHOT = 0,       //单次模式
    MODE_PERIODIC = 1,           //周期模式
} timer_mode;

typedef enum timer_state {
    SOFT_TIMER_STOPPED = 0,      //停止
    SOFT_TIMER_RUNNING = 1,      //运行
    SOFT_TIMER_TIMEOUT = 2,      //超时
} timer_state;

/******************************************************************************
3. 结构体定义
*******************************************************************************/
typedef struct soft_timer {
    timer_state en_timer_state;      // 状态
    timer_mode  en_timer_mode;       // 模式
    uint32_t         ul_period;           // 定时周期
    uint32_t         ul_timeout;          // 到期时间
    soft_timer_callback *cb;         // 回调函数
    void        *param;              // 回调函数参数
} soft_timer_t;

/******************************************************************************
4. 变量声明
*******************************************************************************/

/******************************************************************************
5. 函数声明
*******************************************************************************/
extern void soft_timer_init(void);
extern void soft_timer_update(void);
extern void soft_timer_start(uint16_t us_timer_id, timer_mode en_mode,
    uint32_t ul_delay, soft_timer_callback *cb, void *param);
#endif
