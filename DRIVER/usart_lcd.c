#include "usart_lcd.h"
#include "rs485.h"
#include "sw_timer.h"
#include "delay.h"

#define UART_LCD_CMD_WR_REG         0x80
#define UART_LCD_CMD_RD_REG         0x81

#define UART_LCD_CMD_WR_VAR         0x82
#define UART_LCD_CMD_RD_VAR         0x83

#define UART_LCD_CMD_EXT            0x85

#define USER_R3 0x5A    //帧头
#define USER_RA 0xA5    //帧头

#define USART0_RECV_MAX_LEN         256     // 定义最大接收字节数 256

#define FLOOR_NUM                   7

uint8_t g_USART_TX_BUF[10] = { 0 };

// 每一楼层的密码
uint8_t g_key_array[FLOOR_NUM][6] = {
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
    {0x30, 0x30, 0x00, 0x00, 0x00, 0x00},
};
uint8_t g_new_key_tmp_array[6] = {0x30, 0x30, 0x30, 0x30, 0x30, 0x00};

uint8_t g_key_verify_pass[FLOOR_NUM] = {0};
uint8_t g_key_valid_time[FLOOR_NUM] = {0};
uint8_t g_rc522_verfify_pass = 0;
uint8_t g_is_need_passwd[FLOOR_NUM] = {0};
uint32_t keyVal;
uint32_t keyop_cl;
uint8_t L1,L2,L3,L4,L5,L6,Lop,Lcl;

uint8_t g_led_level = 0;     // 0: 灯关闭， 1：弱， 2： 正常， 3：强
uint8_t g_timer5_cnt = 0;    // timer5每5s统计一次，直到5分钟后, 调暗10.1寸屏幕亮度
uint8_t g_cur_passwd_confirm_floor = 0;
uint8_t g_cur_passwd_modify_floor = 0;
uint8_t g_rgb_changed = 0;
uint8_t g_rgb_val = 0;
uint8_t g_rgb_val_prev = 0;
uint8_t g_light_is_on  = 0;

uint8_t g_usart0_rx_buffer[USART0_RECV_MAX_LEN];     //接收缓冲,最大USART0_RECV_MAX_LEN个字节.
uint16_t g_usart0_start_byte = 0;
uint16_t g_usart0_recv_bytes = 0;

// 风扇状态
uint8_t g_fan_status = 0;    // 0: 表示风扇关闭， 1：表示风扇打开

uint8_t gRdt = 0x1F;
uint8_t gGdt = 0x1F;
uint8_t gBdt = 0x1F;

uint8_t g_floor_1_btn = 0;
uint8_t g_floor_2_btn = 0;
uint8_t g_floor_3_btn = 0;
uint8_t g_floor_4_btn = 0;
uint8_t g_floor_5_btn = 0;
uint8_t g_floor_6_btn = 0;
uint8_t g_open_door_btn = 0;
uint8_t g_close_door_btn = 0;

struct touch_lcd_ctrl_info g_ctrl_info;

void USART0_IRQHandler(void)
{
    g_usart0_rx_buffer[g_usart0_recv_bytes++] = usart_data_receive(USART0);//保存串口数据
    if (g_usart0_recv_bytes == USART0_RECV_MAX_LEN) {
        g_usart0_recv_bytes = 0;
    }
}

void touch_lcd_ctrl_info_ctrl(void)
{
    uint8_t idx;

    g_ctrl_info.open_door_btn = 0;
    g_ctrl_info.close_door_btn = 0;

    g_ctrl_info.fan_ctrl_btn = 1;
    g_ctrl_info.light_ctrl_btn = 1;

    for (idx = 0; idx < 7; idx++) {
        g_ctrl_info.floor_btn[0] = 0;
    }
}

void LEDAllOn(uint8_t idx)
{

}


void sw_timer_callback1(void *param)
{
    g_key_verify_pass[1] = 0;
    g_key_valid_time[1]  = 0;
}

void sw_timer_callback2(void *param)
{
    g_key_verify_pass[2] = 0;
    g_key_valid_time[2]  = 0;
}

void sw_timer_callback3(void *param)
{
    g_key_verify_pass[3] = 0;
    g_key_valid_time[3]  = 0;
}

void sw_timer_callback4(void *param)
{
    g_key_verify_pass[4] = 0;
    g_key_valid_time[4]  = 0;
}

void sw_timer_callback5(void *param)
{
    g_key_verify_pass[5] = 0;
    g_key_valid_time[5]  = 0;
}
void sw_timer_callback6(void *param)
{
    g_key_verify_pass[6] = 0;
    g_key_valid_time[6]  = 0;
}

void USART_LCD_Init(void)
{

}

void USART_LCD_TxShowPageSignal(uint8_t page_num)
{
    uint8_t i = 0;

    g_USART_TX_BUF[0] = 0x5A;
    g_USART_TX_BUF[1] = 0xA5;
    g_USART_TX_BUF[2] = 0x07;
    g_USART_TX_BUF[3] = 0x82;
    g_USART_TX_BUF[4] = 0x00;
    g_USART_TX_BUF[5] = 0x84;
	g_USART_TX_BUF[6] = 0x5a;
	g_USART_TX_BUF[7] = 0x01;
	g_USART_TX_BUF[8] = 0x00;
    g_USART_TX_BUF[9] = page_num & 0xFF; // 编号为page_num的页

    for (i = 0; i < 10; i++) {
        // 向串口0发送数据
        usart_data_transmit(USART0, g_USART_TX_BUF[i]);

        // 等待发送完成
        while (usart_flag_get(USART0, USART_FLAG_TC) != SET);
    }
}

void USART_LCD_PasswdHandle(uint8_t cmd_buff[], uint16_t len)
{
    uint8_t j;
    uint8_t k;
    uint16_t usart_lcd_addr;

    usart_lcd_addr =  (cmd_buff[4] << 8) | cmd_buff[5];

    // 1. 密码确认
    switch(usart_lcd_addr)
    {
        case 0x1060:
            g_cur_passwd_confirm_floor = 1;

            // 表示需要输入密码
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1061:
            g_cur_passwd_confirm_floor = 2;

            // 表示需要输入密码
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1062:
            g_cur_passwd_confirm_floor = 3;

            // 表示需要输入密码
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1063:
            g_cur_passwd_confirm_floor = 4;

            // 表示需要输入密码
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1064:
            g_cur_passwd_confirm_floor = 5;
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1069:
            g_cur_passwd_confirm_floor = 6;
            if ((g_key_verify_pass[g_cur_passwd_confirm_floor] == 0) && (g_is_need_passwd[g_cur_passwd_confirm_floor] == 1))
            {
                // 切换到键盘所在页；A5 5A 04 80 03 00 03
                USART_LCD_TxShowPageSignal(0x3);  // 编号为3的页
                delay_ms(5);
            }
            break;

        case 0x1520:
            if (len == 0x8)
            {
                for (j = 0; j < 4; j++)
                {
                    if (cmd_buff[7 + j] != g_key_array[g_cur_passwd_confirm_floor][2 + j])
                    {
                        break;
                    }
                }

                if (j == 4)
                {
                    g_key_verify_pass[g_cur_passwd_confirm_floor] = 1;
                    g_key_valid_time[g_cur_passwd_confirm_floor]  = 0;

                    // 切换到按钮所在页；A5 5A 04 80 03 00 00
                    USART_LCD_TxShowPageSignal(0x0); // 编号为0的页

                    // 有效期1分钟
                    if (1 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(0, MODE_ONE_SHOT, 10000, sw_timer_callback1, NULL);
                    }
                    else if(2 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(1, MODE_ONE_SHOT, 10000, sw_timer_callback2, NULL);
                    }
                    else if(3 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(2, MODE_ONE_SHOT, 10000, sw_timer_callback3, NULL);
                    }
                    else if(4 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(3, MODE_ONE_SHOT, 10000, sw_timer_callback4, NULL);
                    }

                    else if(5 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(4, MODE_ONE_SHOT, 10000, sw_timer_callback5, NULL);
                    }
                    else if(6 == g_cur_passwd_confirm_floor)
                    {
                        soft_timer_start(5, MODE_ONE_SHOT, 10000, sw_timer_callback6, NULL);
                    }
                    else
                    {
                    }
                }
                else
                {
                    // TODO, 切换到密码不对，请重新输入界面
                }
            }
            else
            {
                // TODO,切换到密码长度不是6位，请重新输入界面
            }
            break;

        case 0x1620:
            if (len == 0x8)
            {
                for (j = 0; j < 4; j++)
                {
                    if (cmd_buff[7 + j] != g_key_array[g_cur_passwd_modify_floor][2+j])
                    {
                        break;
                    }
                }
                if (j == 4)
                {
                    // 切换到按钮所在页；A5 5A 04 80 03 00 06
                    USART_LCD_TxShowPageSignal(0x6); //编号为6的页， 输入新密码的页面
                }
                else
                {
                    // 切换到按钮所在页；A5 5A 04 80 03 00 06
                    USART_LCD_TxShowPageSignal(0x7); //编号为7的页， 表示输入原始密码出错
                }
            }
            break;

        case 0x1720:
            if (len == 0x8)
            {
                for (j = 0; j < 4; j++)
                {
                    g_new_key_tmp_array[2 + j] = cmd_buff[7 + j];
                }

                // 切换到按钮所在页；A5 5A 04 80 03 00 0A
                USART_LCD_TxShowPageSignal(0xA);  // 编号为10的页,表示再一次输入新密码
            }
            else
            {
                // 切换到密码位数不对提示页面；A5 5A 04 80 03 00 09
                USART_LCD_TxShowPageSignal(0x9); // 编号为9的页,表示密码位数不对，只支持6位密码
            }
            break;

        case 0x1820:
            if (len == 0x8)
            {
                for (j = 0; j < 4; j++)
                {
                    if (cmd_buff[7 + j] != g_new_key_tmp_array[2 + j])
                    {
                        break;
                    }
                }

                if (j == 4)
                {
                    // 切换到按钮所在页；A5 5A 04 80 03 00 08
                    USART_LCD_TxShowPageSignal(0x8);  //编号为8的页, 表示新密码修改成功

                    // 保存新密码
                    for (k = 0; k < 6; k++)
                    {
                        g_key_array[g_cur_passwd_modify_floor][k] =g_new_key_tmp_array[k];
                    }
                }
                else
                {
                    // 切换到按钮所在页；A5 5A 04 80 03 00 0B
                    USART_LCD_TxShowPageSignal(0xB); //两次密码不一致提示界面

                }
            }
            else
            {
                // 切换到按钮所在页；A5 5A 04 80 03 00 09
                USART_LCD_TxShowPageSignal(0x9); // 编号为9的页,表示密码位数不对，只支持6位密码
            }
            break;

        case 0x1050:
            g_cur_passwd_modify_floor = 1;
            break;

        case 0x1052:
            g_cur_passwd_modify_floor = 2;
            break;

        case 0x1054:
            g_cur_passwd_modify_floor = 3;
            break;

        case 0x1056:
            g_cur_passwd_modify_floor = 4;
            break;

        case 0x1058:
            g_cur_passwd_modify_floor = 5;
            break;

        case 0x105A:
            g_cur_passwd_modify_floor = 6;
            break;

        default:
            break;
    }
}

void USART_LCD_ButtonHandle(uint8_t cmd_buf[], uint16_t len)
{
    uint16_t usart_lcd_addr;
    uint8_t  cur_passwd_confirm_floor = 0;

    usart_lcd_addr = (cmd_buf[4] << 8) | cmd_buf[5];

    switch(usart_lcd_addr)
    {
        case 0x1060:
            cur_passwd_confirm_floor = 1;
            break;
        case 0x1061:
            cur_passwd_confirm_floor = 2;
            break;
        case 0x1062:
            cur_passwd_confirm_floor = 3;
            break;
        case 0x1063:
            cur_passwd_confirm_floor = 4;
            break;
        case 0x1064:
            cur_passwd_confirm_floor = 5;
            break;
        case 0x1069:
            cur_passwd_confirm_floor = 6;
            break;
        default:
            cur_passwd_confirm_floor = 0;
            break;
    }
    if ((g_key_verify_pass[cur_passwd_confirm_floor] != 0) || (g_is_need_passwd[cur_passwd_confirm_floor] == 0))
    {
        switch(cur_passwd_confirm_floor)
        {
            case 1:
                if  (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_15, SET);
                    keyVal = keyVal & 0xfe;
                    //uart_sw(0x80,0x01);
                }
                else
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_15, RESET);
                    keyVal = keyVal | 0x1;
                    g_ctrl_info.floor_btn[0] |= (1 << 0);
                }
                break;

            case 2:
                if  (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_13, SET);
                    keyVal = keyVal & 0xfd;
                    //uart_sw(0x81,0x02);
                }
                else
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_13, RESET);
                    keyVal = keyVal | 0x02;
                    g_ctrl_info.floor_btn[0] |= (1 << 1);
                }
                break;

            case 3:
                if (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_10, SET);
                    keyVal = keyVal & 0xfb;
                    //uart_sw(0x82,0x03);
                }
                else
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_10, RESET);
                    keyVal = keyVal | 0x04;
                    g_ctrl_info.floor_btn[0] |= (1 << 2);
                }
                break;

            case 4:
                if (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_11, SET);
                    keyVal = keyVal & 0xf7;
                    // uart_sw(0x83,0x04);
                }
                else
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_11, RESET);
                    keyVal = keyVal | 0x08;
                    g_ctrl_info.floor_btn[0] |= (1 << 3);
                }
                break;

            case 5:
                if (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_12, SET);
                    keyVal = keyVal & 0xef;
                    //uart_sw(0x84,0x23);
                }
                else
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_12, RESET);
                    keyVal = keyVal | 0x10;
                    g_ctrl_info.floor_btn[0] |= (1 << 4);
                }
                break;
            case 6:
                if (0x0 == cmd_buf[3 + len - 2])
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_13, SET);
                    keyVal = keyVal & 0xdf;
                              //uart_sw(0x89,0x25);
                }
                else
                {
                    gpio_bit_write(GPIOD, GPIO_PIN_13, RESET);
                    keyVal = keyVal | 0x20;
                    g_ctrl_info.floor_btn[0] |= (1 << 5);
                }
                break;
            default:
                break;
       }
    }
//    auc_rs485_tx_buf[0] = 0x40;
//    auc_rs485_tx_buf[1] = keyVal & 0xff;
//    auc_rs485_tx_buf[2] = 0x00;
//    auc_rs485_tx_buf[3] = keyop_cl & 0xff;
//    ul_crc_check_val = crc_check(auc_rs485_tx_buf, 4);
//    auc_rs485_tx_buf[4] = (ul_crc_check_val ) & 0xff ;
//    i=0;
//    delay_ms(400);
//    rs485_data_tx(auc_rs485_tx_buf, 5);
}

uint8_t g_prev_keyop_cl = 0;
uint8_t g_prev_keyVal = 0;

/*******************************************************************************
函数名: USART_LCD_CmdHandle
功能: 接收到的串口屏串口数据处理
入参: void
出参: void
*******************************************************************************/
void USART_LCD_CmdHandle(uint8_t cmd_buff[], uint16_t len)
{
    uint8_t usart_lcd_cmd;
    uint16_t CRC_VALUE;

    usart_lcd_cmd  = cmd_buff[3];
    switch(usart_lcd_cmd)
    {
        case UART_LCD_CMD_RD_REG:
            break;

        case UART_LCD_CMD_RD_VAR:
            /* 密码处理 */
            USART_LCD_PasswdHandle(cmd_buff, len);
            /* 按钮信息处理 */
            USART_LCD_ButtonHandle(cmd_buff, len);

            if ((0x10 == cmd_buff[4]) && (0x70 == cmd_buff[5]))     // 开启按钮1需要密码
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[1] = 0;
                }
                else
                {
                    g_is_need_passwd[1] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x72 == cmd_buff[5]))    // 开启按钮2需要密码
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[2] = 0;
                }
                else
                {
                    g_is_need_passwd[2] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x74 == cmd_buff[5]))    // 开启按钮3需要密码
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[3] = 0;
                }
                else
                {
                    g_is_need_passwd[3] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x76 == cmd_buff[5]))    // 开启按钮4需要密码
            {
               if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[4] = 0;
                }
                else
                {
                    g_is_need_passwd[4] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x78 == cmd_buff[5]))    // 开启按钮5需要密码
            {
               if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[5] = 0;
                }
                else
                {
                    g_is_need_passwd[5] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x7A == cmd_buff[5]))    // 开启按钮6需要密码
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_is_need_passwd[6] = 0;
                }
                else
                {
                    g_is_need_passwd[6] = 1;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x65 == cmd_buff[5]))    // 电话
            {
                if (0x0 == cmd_buff[3 + len - 2])
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_7, RESET);
                }
                else
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_7, SET);
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x68 == cmd_buff[5]))    // 警铃
            {
                if (0x0 == cmd_buff[3 + len - 2])
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_6, RESET);
                }
                else
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_6, SET);
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x66 == cmd_buff[5]))    // op（190426） 开门?
            {
                if (0x0 == cmd_buff[3 + len - 2])
                {
                    gpio_bit_write(GPIOE, GPIO_PIN_0, SET);
                    keyop_cl = keyop_cl & 0xdf;
                    //uart_sw(0x86,0x15);
                    g_ctrl_info.open_door_btn = 1;
                }
                else
                {
                    gpio_bit_write(GPIOE, GPIO_PIN_0, RESET);
                    keyop_cl = keyop_cl | 0x20;
                    g_ctrl_info.open_door_btn = 0;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x67 == cmd_buff[5]))    // cl（190426）关门？
            {
                  if (0x0 == cmd_buff[3 + len - 2])
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_9, SET);
                    keyop_cl = keyop_cl & 0xbf;
                    //uart_sw(0x87,0x17);
                    g_ctrl_info.close_door_btn = 1;
                }
                else
                {
                    gpio_bit_write(GPIOB, GPIO_PIN_9, RESET);
                    keyop_cl = keyop_cl | 0x40;
                    g_ctrl_info.close_door_btn = 0;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x08 == cmd_buff[5]))    // 警铃
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_6, RESET);
                }
                else
                {
                    gpio_bit_write(GPIOC, GPIO_PIN_6, SET);
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x10 == cmd_buff[5]))    // 风扇
            {
                if (0x0 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_fan_status = 0;           //0是关闭风扇
                    gpio_bit_write(GPIOC, GPIO_PIN_9, RESET);
                    g_ctrl_info.fan_ctrl_btn = 1;
                }
                else
                {
                    g_fan_status = 1;           //1是打开风扇
                    gpio_bit_write(GPIOC, GPIO_PIN_9, SET);
                    g_ctrl_info.fan_ctrl_btn = 0;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x12 == cmd_buff[5]))    // 灯亮度等级设置
            {
                if (0x1 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_led_level = 1;

                    gRdt = 0x0B;
                    gGdt = 0x0B;
                    gBdt = 0x0B;
                }
                else if (0x2 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_led_level = 2;

                    gRdt = 0x15;
                    gGdt = 0x15;
                    gBdt = 0x15;
                }
                else if (0x3 == (cmd_buff[3 + len - 1] & 0x0F))
                {
                    g_led_level = 3;

                    gRdt = 0x1F;
                    gGdt = 0x1F;
                    gBdt = 0x1F;
                }
                else
                {
                    g_led_level = 0;

                    gRdt = 0x0;
                    gGdt = 0x0;
                    gBdt = 0x0;
                }

                if (0 == g_led_level)
                {
                    g_light_is_on = 0;
                }
                else
                {
                    g_light_is_on = 1;
                }

                LEDAllOn(g_rgb_val_prev);
            }
            else if ((0x10 == cmd_buff[4]) && (0x16 == cmd_buff[5]))    // 灯颜色选择: 绿色
            {
                g_rgb_changed = 1;

                if (0x0 != (cmd_buff[3 + len - 1] & 0xFF))
                {
                    g_rgb_val = g_rgb_val | 0x04;
                    gRdt      = cmd_buff[3 + len - 1] & 0xFF;

                }
                else
                {
                    g_rgb_val = g_rgb_val & 0x03;
                    gRdt      = 0x0;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x14 == cmd_buff[5]))    //灯颜色选择: 红色
            {
                g_rgb_changed = 1;
                if (0x0 != (cmd_buff[3 + len - 1] & 0xFF))
                {
                    g_rgb_val = g_rgb_val | 0x02;
                    gGdt      = cmd_buff[3 + len - 1] & 0xFF;
                }
                else
                {
                    g_rgb_val = g_rgb_val & 0x05;
                    gGdt      = 0x0;
                }
            }
            else if ((0x10 == cmd_buff[4]) && (0x18 == cmd_buff[5]))    //灯颜色选择: 蓝色
            {
                g_rgb_changed = 1;
                if (0x0 != (cmd_buff[3 + len - 1] & 0xFF))
                {
                    g_rgb_val = g_rgb_val | 0x01;
                    gBdt      = cmd_buff[3 + len - 1] & 0xFF;
                }
                else
                {
                    g_rgb_val = g_rgb_val & 0x06;
                    gBdt      = 0x0;
                }
            }
            if (g_rgb_changed == 1)
            {
                g_rgb_changed = 0;

                g_rgb_val_prev = g_rgb_val;

                LEDAllOn(g_rgb_val_prev);

                if (0 == g_led_level)
                {
                    g_light_is_on = 0;
                }
                else
                {
                    g_light_is_on = 1;
                }
            }
            break;
        default:////命令无效,删除
            break;
    }

//    if ((g_prev_keyop_cl != keyop_cl) || (g_prev_keyVal != keyVal))
//    {
//        g_prev_keyop_cl = keyop_cl;
//        g_prev_keyVal =  keyVal;
//        g_rs485_tx_buffer[0] = 0x5c;
//        g_rs485_tx_buffer[1] = keyop_cl & 0xff;
//        g_rs485_tx_buffer[2] = keyVal & 0xff;
//        g_rs485_tx_buffer[3] = 0x00;
//        g_rs485_tx_buffer[4] = 0x00;
//        g_rs485_tx_buffer[5] = 0x00;
//        g_rs485_tx_buffer[6] = 0x00;
//        g_rs485_tx_buffer[7] = 0x00;
//        g_rs485_tx_buffer[8] = 0x00;
//        g_rs485_tx_buffer[9] = 0x00;

//        CRC_VALUE = RS485_CrcCheck(g_rs485_tx_buffer, 10);
//        g_rs485_tx_buffer[10] =  (uint8_t)((CRC_VALUE >> 8) & 0xff);
//        g_rs485_tx_buffer[11] =  (uint8_t)( CRC_VALUE & 0xff);

//        RS485_SendData(g_rs485_tx_buffer, 12);
//    }
}

/*******************************************************************************
函数名: usart1_rx_proc
功能: 串口1接收数据处理接口
入参: void
出参: void
*******************************************************************************/
void USART_LCD_RxProc(void)
{
    uint16_t i, CurNum, tem_TalNum;

    uint8_t cmd_buff[256];
    uint16_t len;
    uint16_t nowbuffer;

    len = g_usart0_start_byte;
    tem_TalNum = g_usart0_recv_bytes;

    if (tem_TalNum == len)
    {
       return;
    }

    if (g_usart0_rx_buffer[g_usart0_start_byte] != USER_R3)
    {
        g_usart0_start_byte++;
        if (g_usart0_start_byte == USART0_RECV_MAX_LEN)
        {
            g_usart0_start_byte=0;
        }
        return;
    }
    if (tem_TalNum > len)
    {
        nowbuffer = tem_TalNum - len;
    }
    else
    {
        nowbuffer = tem_TalNum + USART0_RECV_MAX_LEN - len;
    }

    if (nowbuffer < 5)
    {
        return;
    }

    CurNum = g_usart0_start_byte + 2;

    if (CurNum > USART0_RECV_MAX_LEN - 1)
    {
        CurNum -= USART0_RECV_MAX_LEN;
    }
    len = g_usart0_rx_buffer[CurNum] + 3;
    if (nowbuffer < len)
    {
        return;
    }

    i = 0;
    CurNum = g_usart0_start_byte;
    while (1)
    {
        cmd_buff[i++] = g_usart0_rx_buffer[CurNum++];
        if (CurNum == USART0_RECV_MAX_LEN)
        {
            CurNum=0;
        }
        if (i == 4)
        {
            if((cmd_buff[0] != USER_R3) || (cmd_buff[1] != USER_RA))//
            {
                g_usart0_start_byte = CurNum;
                return;
            }
            len = cmd_buff[2];
        }
        else if (i > 4)
        {
            if (i == (len + 3))
            {
                g_usart0_start_byte=CurNum;
                break;
            }
            else if (i > 255)
            {
                g_usart0_start_byte = CurNum;
                return;
            }
            else if (CurNum == tem_TalNum)
            {
                return;
            }
        }
    }

    USART_LCD_CmdHandle(cmd_buff, len);
}

void USART_LCD_ShowFloorNum(uint8_t floor_num)
{
    uint8_t idx;

    g_USART_TX_BUF[0] = 0x5A;
    g_USART_TX_BUF[1] = 0xA5;
    g_USART_TX_BUF[2] = 0x05;
    g_USART_TX_BUF[3] = 0x82;
    g_USART_TX_BUF[4] = 0x30;
    g_USART_TX_BUF[5] = 0x00;  // 楼层显示变量地址
    g_USART_TX_BUF[6] = 0x00;
    g_USART_TX_BUF[7] = floor_num & 0xff;

    for (idx = 0; idx < 8; idx++)
    {
        // 向串口0发送数据
        usart_data_transmit(USART0, g_USART_TX_BUF[idx]);

        // 等待发送完成
        while (usart_flag_get(USART0, USART_FLAG_TC) != SET);
    }
}

void USART_LCD_ShowDirArrow(uint8_t show_dir)
{
    uint8_t idx;
    uint8_t ud_flag;
    uint8_t ud_flag1;

    ud_flag = show_dir & 0x01;
    ud_flag1 = show_dir & 0x06;

    g_USART_TX_BUF[0] = 0x5A;
    g_USART_TX_BUF[1] = 0xA5;
    g_USART_TX_BUF[2] = 0x05;
    g_USART_TX_BUF[3] = 0x82;
    g_USART_TX_BUF[4] = 0x20;
    g_USART_TX_BUF[5] = 0x00;  // 楼层显示变量地址
    g_USART_TX_BUF[6] = 0x00;

    if ((ud_flag == 0x00) && (ud_flag1 != 0x06))    // 向上箭头
    {
        g_USART_TX_BUF[7] = 0x1;
    }
    if ((ud_flag == 0x01) && (ud_flag1 != 0x06))   // 向下箭头
    {
        g_USART_TX_BUF[7] = 0x2;
    }
    if(ud_flag1 == 0x06)
    {
       g_USART_TX_BUF[7] = 0x0;
    }

    for (idx = 0; idx < 8; idx++)
    {
        // 向串口0发送数据
        usart_data_transmit(USART0, g_USART_TX_BUF[idx]);

        // 等待发送完成
        while (usart_flag_get(USART0, USART_FLAG_TC) != SET);
    }
}

uint8_t button_on_num[9] = {0};
uint8_t is_send[9] = {0};
uint8_t is_send_ok[9] = {0};
void USART_LCD_SetButtonStatus(void)
{
    uint8_t idx = 0;
    uint8_t t;

    //if ((GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14) == 0) || (L1 ==0x1))  // 1
    if (g_floor_1_btn == 0x1)
    {
        is_send[0] = 1;
        button_on_num[0] = 1;
    }
    else
    {
        is_send[0] = 0;
        button_on_num[0] = 0;
    }

    //if ((GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12) == 0) || (L2 ==0x2))    // 2
    if (g_floor_2_btn == 0x2)
    {
        is_send[1] = 1;
        button_on_num[1] = 3;
    }
    else
    {
        is_send[1] = 0;
        button_on_num[1] = 0;
    }

    // if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_2) == 0) || (L3 ==0x4))      // 3
    if (g_floor_3_btn == 0x4)
    {
        is_send[2] = 1;
        button_on_num[2] = 5;
    }
    else
    {
        is_send[2] = 0;
        button_on_num[2] = 0;
    }

    // if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_3) == 0)|| (L4 ==0x8))      // 4
    if (g_floor_4_btn == 0x8)
    {
        is_send[3] = 1;
        button_on_num[3] = 7;
    }
    else
    {
        is_send[3] = 0;
        button_on_num[3] = 0;
    }

    //if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_4) == 0) || (L5 ==0x10))     // 5
    if (g_floor_5_btn == 0x10)
    {
        is_send[4] = 1;
        button_on_num[4] = 23;
    }
    else
    {
        is_send[4] = 0;
        button_on_num[4] = 0;
    }

    //if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_5) == 0) || (L6 ==0x20))     // 6
    if (g_floor_6_btn == 0x20)
    {
        is_send[5] = 1;
        button_on_num[5] = 25;
    }
    else
    {
        is_send[5] = 0;
        button_on_num[5] = 0;
    }

    //if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_6) == 0)|| (Lop ==0x1))      // 开门
    if (g_open_door_btn == 0x20)
    {
        is_send[6] = 1;
        button_on_num[6] = 15;
    }
    else
    {
        is_send[6] = 0;
        button_on_num[6] = 0;
    }

    //if ((GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_7) == 0)|| (Lcl ==0x2))      // 关门
    if (g_close_door_btn == 0x40)
    {
        is_send[7] = 1;
        button_on_num[7] = 17;
    }
    else
    {
        is_send[7] = 0;
        button_on_num[7] = 0;
    }

    for (idx = 0; idx < 8; idx++)
    {
        if ((1 == is_send[idx]) && (0 == is_send_ok[idx]))
        {
            is_send_ok[idx] = 1;

            g_USART_TX_BUF[0] = 0x5A;
            g_USART_TX_BUF[1] = 0xA5;
            g_USART_TX_BUF[2] = 0x05;
            g_USART_TX_BUF[3] = 0x82;
            g_USART_TX_BUF[4] = 0x10;

            if (idx == 5) {
                g_USART_TX_BUF[5] = 0x89;
            } else {
                g_USART_TX_BUF[5] = 0x80 + idx;  // 对应的按钮变量地址
            }

            g_USART_TX_BUF[6] = 0x00;
            g_USART_TX_BUF[7] = button_on_num[idx];

            for (t = 0; t < 8; t++)
            {
                // 向串口0发送数据
                usart_data_transmit(USART0, g_USART_TX_BUF[t]);

                // 等待发送完成
                while (usart_flag_get(USART0, USART_FLAG_TC) != SET);
            }
        }
        else if ((0 == is_send[idx]) && (1 == is_send_ok[idx]))
        {
            is_send_ok[idx] = 0;

            g_USART_TX_BUF[0] = 0x5A;
            g_USART_TX_BUF[1] = 0xA5;
            g_USART_TX_BUF[2] = 0x05;
            g_USART_TX_BUF[3] = 0x82;
            g_USART_TX_BUF[4] = 0x10;

            if (idx == 5) {
                g_USART_TX_BUF[5] = 0x89;
            } else {
                g_USART_TX_BUF[5] = 0x80 + idx;  // 对应的按钮变量地址
            }

            g_USART_TX_BUF[6] = 0x00;
            g_USART_TX_BUF[7] = button_on_num[idx];

            for(t = 0; t < 8; t++)
            {
                // 向串口0发送数据
                usart_data_transmit(USART0, g_USART_TX_BUF[t]);

                // 等待发送完成
                while (usart_flag_get(USART0, USART_FLAG_TC) != SET);
            }
        }
        else
        {

        }
    }
}
