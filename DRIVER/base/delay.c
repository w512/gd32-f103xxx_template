#include "delay.h"

static uint8_t  fac_us = 0;   //us延时倍乘数
static uint16_t fac_ms = 0;   //ms延时倍乘数,在ucos下,代表每个节拍的ms数

//初始化延迟函数
//当使用OS的时候,此函数会初始化OS的时钟节拍
//SYSTICK的时钟固定为HCLK时钟的1/8
//SYSCLK:系统时钟
void Delay_Init()
{
    fac_us = SystemCoreClock / 8000000;     // 为系统时钟的1/8

    fac_ms = (uint16_t)fac_us * 1000;       // 非OS下,代表每个ms需要的systick时钟数
}

//延时(us_num)us
//us_num为要延时的us数.
void delay_us(uint32_t us_num)
{
    uint32_t temp;

    SysTick->LOAD = us_num * fac_us;            //时间加载
    SysTick->VAL  = 0x00;                       //清空计数器
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;   //开始倒数

    do
    {
        temp = SysTick->CTRL;
    } while ((temp & 0x01) && !(temp & (1 << 16)));     //等待时间到达

    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;          //关闭计数器
    SysTick->VAL = 0x00;                                //清空计数器
}

//延时num_ms
//注意num_ms的范围
//SysTick->LOAD为24位寄存器,所以,最大延时为:
//num_ms <= 0xffffff*8*1000/SYSCLK
//SYSCLK单位为Hz, num_ms单位为ms
//对72M条件下, num_ms <= 1864
void delay_ms(uint16_t num_ms)
{
    uint32_t temp;
    SysTick->LOAD = (uint32_t)num_ms * fac_ms;    // 时间加载(SysTick->LOAD为24bit)
    SysTick->VAL  = 0x00;                         // 清空计数器
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;     // 开始倒数

    do
    {
        temp = SysTick->CTRL;
    } while ((temp & 0x01) && !(temp & (1 << 16)));   // 等待时间到达

    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;      // 关闭计数器
    SysTick->VAL = 0x00;                            // 清空计数器
}
