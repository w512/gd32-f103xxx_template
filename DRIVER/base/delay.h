#ifndef __DELAY_H__
#define __DELAY_H__

#include "gd32f10x.h"

void Delay_Init(void);
void delay_ms(uint16_t nms);
void delay_us(uint32_t nus);

#endif
