#include "nvic.h"

void NVIC_Init(void)
{
    /*
     * PART 1 - USART INTERRUPT CONFIGURE
     */
    /* USART NVIC 配置 */
    // nvic_irq_enable(USART0_IRQn, 2, 0);
    nvic_irq_enable(USART0_IRQn, 3, 3);   // IRQ通道使能, 抢占优先级3, 子优先级3
    nvic_irq_enable(USART1_IRQn, 3, 3);   // IRQ通道使能, 抢占优先级3, 子优先级3

    // 打开 USART0 接收中断
    usart_interrupt_enable(USART0, USART_INT_RBNE);
    // 打开 USART1 接收中断
    usart_interrupt_enable(USART1, USART_INT_RBNE);

    /*
     * PART 2 - CAN INTERRUPT CONFIGURE
     */
    nvic_irq_enable(USBD_LP_CAN0_RX0_IRQn, 0, 0);

    /* enable CAN receive FIFO0 not empty interrupt */
    can_interrupt_enable(CAN0, CAN_INT_RFNE0);

    /*
     * PART 3 - TIMER INTERRUPT CONFIGURE
     */
    nvic_irq_enable(TIMER5_IRQn, 3, 3);             // Timer 5中断设置, 抢占优先级3, 子优先级3
    timer_interrupt_flag_clear(TIMER5, TIMER_INT_FLAG_UP);
    timer_interrupt_enable(TIMER5, TIMER_INT_UP);

    nvic_irq_enable(TIMER2_IRQn, 3, 3);             // Timer 5中断设置, 抢占优先级3, 子优先级3
    timer_interrupt_flag_clear(TIMER2, TIMER_INT_FLAG_UP);
    timer_interrupt_enable(TIMER2, TIMER_INT_UP);
}
