#include "gpio.h"

#define USART0_PORT     GPIOA
#define USART0_TX_PIN   GPIO_PIN_9
#define USART0_RX_PIN   GPIO_PIN_10

#define USART1_PORT     GPIOA
#define USART1_TX_PIN   GPIO_PIN_2
#define USART1_RX_PIN   GPIO_PIN_3

#define USART2_PORT     GPIOC
#define USART2_TX_PIN   GPIO_PIN_10
#define USART2_RX_PIN   GPIO_PIN_11

#define CAN0_PORT       GPIOA
#define CAN0_TX_PIN     GPIO_PIN_12
#define CAN0_RX_PIN     GPIO_PIN_11

#define RS485_TX_EN     PAout(15)   // 485模式控制.0,接收;1,发送

void GPIO_Init(void)
{
    /**
     * PART 1: USART GPIO初始化
     */
    /* USART0_TX => GPIOA.9 */
    // 初始化GPIOA.9, 复用推挽输出,
    gpio_init(USART0_PORT, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, USART0_TX_PIN);

    /* USART0_RX => GPIOA.10 */
    // 初始化GPIOA.10, 浮空输入
    gpio_init(USART0_PORT, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, USART0_RX_PIN);

    /* USART0_TX => GPIOA.2 */
    // 初始化GPIOA.2, 复用推挽输出,
    gpio_init(USART1_PORT, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, USART1_TX_PIN);

    /* USART1_RX => GPIOA.3 */
    // 初始化GPIOA.3, 浮空输入
    gpio_init(USART1_PORT, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, USART1_RX_PIN);

    // 使用SW下载, 不使用JTAG下载, 管脚用作其他功能
    gpio_pin_remap_config(GPIO_SWJ_SWDPENABLE_REMAP, ENABLE);
    //gpio_pin_remap_config(GPIO_USART2_PARTIAL_REMAP, ENABLE);

    //gpio_init(USART2_PORT, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, USART2_TX_PIN);

    /* USART2_RX => GPIOA.10 */
    // 初始化GPIOA.10, 浮空输入
    //gpio_init(USART2_PORT, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, USART2_RX_PIN);

    /*
     * PART 2: CAN GPIO初始化
     */
    gpio_init(CAN0_PORT, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, CAN0_TX_PIN);
    gpio_init(CAN0_PORT, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, CAN0_RX_PIN);

    // 485 TX与RX切换引脚
    gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_15);
    gpio_bit_reset(GPIOA, GPIO_PIN_15); // 485默认切到接收状态
}
