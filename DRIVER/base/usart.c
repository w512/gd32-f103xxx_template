#include "usart.h"

/*
 * 函数名: Usart1Init
 * 功能: 串口初始化
 * 入参: 
        usart_periph: 串口类型
        baud_rate: 波特率
 * 出参: void
 */
void USARTx_Init(uint32_t usart_periph, uint32_t baud_rate)
{
    /* 复位USART0 */
    usart_deinit(usart_periph);

    usart_baudrate_set(usart_periph, baud_rate);                      // 串口波特率
    usart_word_length_set(usart_periph, USART_WL_8BIT);               // 字长为8位数据格式
    usart_stop_bit_set(usart_periph, USART_STB_1BIT);                 // 一个停止位
    usart_parity_config(usart_periph, USART_PM_NONE);                 // 无奇偶校验位
    usart_hardware_flow_rts_config(usart_periph, USART_RTS_DISABLE);  // 无硬件数据流控制
    usart_hardware_flow_cts_config(usart_periph, USART_CTS_DISABLE);
    usart_receive_config(usart_periph, USART_RECEIVE_ENABLE);         // 收发模式
    usart_transmit_config(usart_periph, USART_TRANSMIT_ENABLE);
    
    /* 使能USART0 */
    usart_enable(usart_periph);
}

void USART_Init(void)
{
    // USART0串口初始化, 波特率设置为115200
    USARTx_Init(USART0, 115200);

    // USART1串口初始化, 波特率设置为38400
    USARTx_Init(USART1, 38400);
}
