#ifndef __CAN_H__
#define __CAN_H__

#include "gd32f10x.h"
#include "gd32f10x_can.h"

extern FlagStatus can_recv_flag;

void CAN_Init(void);

#endif
