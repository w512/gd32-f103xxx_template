#include "clock.h"

void SYSCLK_Init(void)
{
    rcu_periph_clock_enable(RCU_AF);

    /* 使能GPIO时钟 */
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_GPIOC);
    rcu_periph_clock_enable(RCU_GPIOD);
    rcu_periph_clock_enable(RCU_GPIOE);

    // 使能USART时钟
    rcu_periph_clock_enable(RCU_USART0);
    rcu_periph_clock_enable(RCU_USART1);

    // 使能DMA0时钟
    //rcu_periph_clock_enable(RCU_DMA0);

    // 使能CAN0时钟
    rcu_periph_clock_enable(RCU_CAN0);

    // 使能TIMER时钟
    rcu_periph_clock_enable(RCU_TIMER2);
    rcu_periph_clock_enable(RCU_TIMER5);
}
