#include "timer.h"

void Timer5_Init(uint16_t arr, uint16_t psc)
{
    timer_parameter_struct timer_init_para;

    timer_deinit(TIMER5);

    /* initialize TIMER init parameter struct */
    timer_struct_para_init(&timer_init_para);

    timer_init_para.period            = arr;    /* 自动重装载值 */
    timer_init_para.prescaler         = psc;    /* 时钟预分频系数 */
    timer_init_para.clockdivision     = TIMER_CKDIV_DIV1;
    timer_init_para.alignedmode       = TIMER_COUNTER_EDGE;
    timer_init_para.counterdirection  = TIMER_COUNTER_UP;
    timer_init(TIMER5, &timer_init_para);

    timer_enable(TIMER5);
}

void Timer2_Init(uint16_t arr, uint16_t psc)
{
    timer_parameter_struct timer_init_para;

    timer_deinit(TIMER2);

    /* initialize TIMER init parameter struct */
    timer_struct_para_init(&timer_init_para);

    timer_init_para.period            = arr;    /* 自动重装载值 */
    timer_init_para.prescaler         = psc;    /* 时钟预分频系数 */
    timer_init_para.clockdivision     = TIMER_CKDIV_DIV1;
    timer_init_para.alignedmode       = TIMER_COUNTER_EDGE;
    timer_init_para.counterdirection  = TIMER_COUNTER_UP;
    timer_init(TIMER2, &timer_init_para);

    timer_enable(TIMER2);
}

void TIMER_Init(void)
{
    Timer2_Init(10 - 1, 7200 - 1);    // 定时500ms

    Timer5_Init(5000 - 1, 7200 - 1);    // 定时500ms
}

void TIMER5_IRQHandler(void)
{
    if (timer_interrupt_flag_get(TIMER5, TIMER_INT_FLAG_UP) != RESET) {
        /* clear channel 0 interrupt bit */
        timer_interrupt_flag_clear(TIMER5, TIMER_INT_FLAG_UP);
    }
}
