#include "can.h"

FlagStatus can_recv_flag;
can_receive_message_struct can_recv_msg;

void CAN0_Init(uint8_t tsjw, uint8_t tbs2, uint8_t tbs1, uint16_t brp, uint8_t mode)
{
    can_parameter_struct can_parameter;
    can_filter_parameter_struct can_filter;
    /* initialize CAN register */
    can_deinit(CAN0);
    
    /* initialize CAN */
    can_parameter.time_triggered = DISABLE;
    can_parameter.auto_bus_off_recovery = DISABLE;
    can_parameter.auto_wake_up = DISABLE;
    can_parameter.no_auto_retrans = DISABLE;
    can_parameter.rec_fifo_overwrite = DISABLE;
    can_parameter.trans_fifo_order = DISABLE;
    can_parameter.working_mode = mode;
    can_parameter.resync_jump_width = tsjw;
    can_parameter.time_segment_1 = tbs1;
    can_parameter.time_segment_2 = tbs2;
    /* baudrate 1Mbps */
    can_parameter.prescaler = brp;
    can_init(CAN0, &can_parameter);

    /* initialize filter */
    /* CAN0 filter number */
    can_filter.filter_number = 0;

    /* initialize filter */
    can_filter.filter_mode = CAN_FILTERMODE_MASK;
    can_filter.filter_bits = CAN_FILTERBITS_32BIT;
    can_filter.filter_list_high = 0x0000;
    can_filter.filter_list_low = 0x0000;
    can_filter.filter_mask_high = 0x0000;
    can_filter.filter_mask_low = 0x0000;  
    can_filter.filter_fifo_number = CAN_FIFO0;
    can_filter.filter_enable = ENABLE;
    can_filter_init(&can_filter);
}

void CAN_Init(void)
{
    CAN0_Init(CAN_BT_SJW_1TQ, CAN_BT_BS2_8TQ, CAN_BT_BS1_9TQ, 40, CAN_NORMAL_MODE);
}

/*!
    \brief      this function handles CAN0 RX0 exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USBD_LP_CAN0_RX0_IRQHandler(void)
{
    /* check the receive message */
    can_message_receive(CAN0, CAN_FIFO0, &can_recv_msg);
    if((0x321 == can_recv_msg.rx_sfid)&&(CAN_FF_STANDARD == can_recv_msg.rx_ff) && (2 == can_recv_msg.rx_dlen)){
        can_recv_flag = SET;
    }
}
