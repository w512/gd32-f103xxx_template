#ifndef __USART_LCD_H__
#define __USART_LCD_H__

#include "gd32f10x.h"

struct touch_lcd_ctrl_info {
    uint8_t open_door_btn;
    uint8_t close_door_btn;
    uint8_t floor_btn[7]; // 对应7 * 8 = 56楼
    uint8_t fan_ctrl_btn;
    uint8_t light_ctrl_btn;
};

extern uint8_t g_floor_1_btn;
extern uint8_t g_floor_2_btn;
extern uint8_t g_floor_3_btn;
extern uint8_t g_floor_4_btn;
extern uint8_t g_floor_5_btn;
extern uint8_t g_floor_6_btn;
extern uint8_t g_open_door_btn;
extern uint8_t g_close_door_btn;

extern struct touch_lcd_ctrl_info g_ctrl_info;

void USART_LCD_Init(void);
void USART_LCD_RxProc(void);
void USART_LCD_SetButtonStatus(void);

void USART_LCD_ShowFloorNum(uint8_t floor_num);
void USART_LCD_ShowDirArrow(uint8_t show_dir);

void touch_lcd_ctrl_info_ctrl(void);

#endif
