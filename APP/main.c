/*!
    \file    main.c
    \brief   GPIO running led demo

    \version 2014-12-26, V1.0.0, demo for GD32F10x
    \version 2017-06-30, V2.0.0, demo for GD32F10x
    \version 2021-04-30, V2.1.0, demo for GD32F10x
*/

/*
    Copyright (c) 2021, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software without
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.
*/

#include "gd32f10x.h"
#include "delay.h"
#include "usart.h"
#include "clock.h"
#include "gpio.h"
#include "nvic.h"
#include "can.h"
#include "timer.h"
#include "usart_lcd.h"
#include "rs485.h"
#include "sw_timer.h"

/*
 * 功能: 系统源时钟选择
 */
void SystemClkSourceSelect(void)
{
    systick_clksource_set(SYSTICK_CLKSOURCE_HCLK_DIV8); // 选择外部时钟, HCLK/8
}

/*
 * 功能: 系统中断初始化
 */
void SystemInterruptInit(void)
{
    /* 设置NVIC中断分组2:2位抢占优先级，2位响应优先级 */
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
}

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/

int main(void)
{
    uint8_t rs485_rx_buf[32];
    uint8_t rs485_rx_len = 0;

    /* 系统时钟源选择 */
    SystemClkSourceSelect();

    /* 延时函数初始化 */
    Delay_Init();

    /* 中断配置 */
    SystemInterruptInit();

    SYSCLK_Init();
    GPIO_Init();
    USART_Init();
    CAN_Init();
    TIMER_Init();
    NVIC_Init();

    touch_lcd_ctrl_info_ctrl();

    soft_timer_init();
    while (1) {
        /* turn on LED */
        // gpio_bit_set(GPIOC, GPIO_PIN_6);
        // /* insert 500 ms delay */
        // delay_ms(500);

        // /* turn off LED */
        // gpio_bit_reset(GPIOC, GPIO_PIN_6);

        // /* insert 500 ms delay */
        // delay_ms(500);

        USART_LCD_RxProc();

        USART_LCD_SetButtonStatus();

        // RS485接收数据
        RS485_RecvData(rs485_rx_buf, &rs485_rx_len);

        if (rs485_rx_len >= 8)
        {
            RS485_RecvDataHandle(rs485_rx_buf, 8);
        }
    }
}
